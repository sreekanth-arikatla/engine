# Scripts to assist developers
# Mostly these are calling the test suite driver and validator classes in Java

# TODO More Test Targets
# Rebasing
# mil.tatrc.physiology.utilities.testing.Rebase [from to (config|ALL)]
#
# Ploting
# mil.tatrc.physiology.utilities.csv.plots.PlotDriver
#
# Reconfigure the config file (in memory) so we can test serialization
# mil.tatrc.physiology.utilities.testing.Reconfiguration

find_package(Java REQUIRED)
include(UseJava)
if(_JAVA_HOME)
  # Overwrite Java if the env variable is set
  
  message(STATUS "Using JAVA_HOME as my Java executable (set to : ${_JAVA_HOME})")
  set(Java_JAVA_EXECUTABLE ${_JAVA_HOME}/bin/java)
endif()
#  You tell me what JDK/JRE you want to use
#set(Java_JAVA_EXECUTABLE "/the_jdk_I_want/bin/java)

set(JAVA_CLASSPATH BioGears.jar
        @DATA_ROOT@/jar/javassist-3.16.1-GA.jar
        @DATA_ROOT@/jar/jcommon-1.0.16.jar
        @DATA_ROOT@/jar/jdom-2.0.2.jar
        @DATA_ROOT@/jar/jfreechart-1.0.13.jar
        @DATA_ROOT@/jar/guava-11.0.2.jar
        @DATA_ROOT@/jar/log4j-1.2.17.jar
        @DATA_ROOT@/jar/poi-3.13-20150929.jar
        @DATA_ROOT@/jar/poi-ooxml-3.13-20150929.jar
        @DATA_ROOT@/jar/poi-ooxml-schemas-3.13-20150929.jar
        @DATA_ROOT@/jar/pdfbox-2.0.0-RC3.jar
        @DATA_ROOT@/jar/reflections-0.9.9-RC1-uberjar.jar
        @DATA_ROOT@/jar/xmlbeans-2.6.0.jar
        @DATA_ROOT@/jar/zip4j-1.3.1.jar)

if(NOT WIN32)
  string(REPLACE ";" ":" JAVA_CLASSPATH "${JAVA_CLASSPATH}")
endif()


if(TYPE STREQUAL "SystemValidation")
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.testing.TestDriver ValidationSystems.config)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.testing.validation.SystemValidation )
elseif(TYPE STREQUAL "PatientValidation")
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.testing.TestDriver ValidationPatients.config)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.testing.validation.PatientValidation)
elseif(TYPE STREQUAL "genData")
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.biogears.dataset.DataSetReader)
elseif(TYPE STREQUAL "genStates")
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.testing.TestDriver InitialPatientStates.config)
elseif(TYPE STREQUAL "doxygen")
  file(MAKE_DIRECTORY  "./docs/html")
  file(MAKE_DIRECTORY  "./docs/markdown")
  file(GLOB_RECURSE DOCS "./docs/html/*.*" "./docs/markdown/*.*")
  if(DOCS)
    file(REMOVE ${DOCS})
  endif()
  file(COPY C:/Programming/physiology/src/docs/Doxygen/bootstrap DESTINATION ./docs/html)
  file(COPY C:/Programming/physiology/src/docs/Doxygen/font-awesome-4.3.0 DESTINATION ./docs/html)
  file(COPY C:/Programming/physiology/src/docs/Images DESTINATION ./docs/html)
  # These are run above
  #execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.testing.validation.SystemValidation)
  #execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.testing.validation.PatientValidation)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.datamodel.doxygen.DoxygenPreprocessor @DATA_ROOT@/docs/Markdown ./docs/markdown)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.datamodel.doxygen.DoxygenPreprocessor @DATA_ROOT@/docs/Methodology ./docs/markdown)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.datamodel.doxygen.XSDToDoxygen ./xsd/BioGearsDataModel.xsd ./docs/markdown/xsd.ixx)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.datamodel.doxygen.CDM2MD ./docs/markdown)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.utilities.csv.plots.PlotDriver PlotRun.config)
  execute_process(COMMAND doxygen ./docs/full.doxy)
else()
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -Xmx900m -classpath "${JAVA_CLASSPATH}" mil.tatrc.physiology.testing.TestDriver ${TYPE}.config)
endif()
